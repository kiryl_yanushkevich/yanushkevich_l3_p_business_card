package com.example.yanushkevich_l3_p_business_card;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class EmailDialogFragment extends DialogFragment {
    private static final String TAG = "EmailDialogFragment";

    public interface onEmailPermission{
        void sendEmailPermission(boolean answer);
    }

    public onEmailPermission permissionInterface;

    private TextView title;
    private TextView mainText;

    private Button okButton;
    private Button cancelButton;

    private boolean permission;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_window_fragment, container, false);
        title = view.findViewById(R.id.dialog_window_title);
        mainText = view.findViewById(R.id.dialog_window_main_text);
        okButton = view.findViewById(R.id.dialog_window_ok_button);
        cancelButton = view.findViewById(R.id.dialog_window_no_button);

        title.setText(R.string.email_dialog_window_title);
        EditText phoneNumber = getActivity().findViewById(R.id.email_text_field);

        String number = phoneNumber.getText().toString();

        mainText.setText(number);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permission = true;
                permissionInterface.sendEmailPermission(permission);
                getDialog().dismiss();
            }
        });


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                permission = false;
                permissionInterface.sendEmailPermission(permission);
                getDialog().dismiss();
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            permissionInterface = (EmailDialogFragment.onEmailPermission) getActivity();
        } catch (ClassCastException e){
            Log.e(TAG, "onAttach: " + e.getMessage());
        }
    }
}
