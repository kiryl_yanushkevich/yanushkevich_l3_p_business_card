package com.example.yanushkevich_l3_p_business_card;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements PhoneDialogFragment.onPhonePermission, EmailDialogFragment.onEmailPermission {

    private EditText nameTextField;
    private EditText phoneNumber;
    private EditText emailTextField;

    private Button callButton;
    private Button emailButton;
    private Button saveEditButton;
    private Button saveFileButton;
    private Button loadFileButton;

    private String fileName = "testFile.txt";

    ImageView avatarImage;

    String pathImage = "";
    Person person;

    private static final int REQUEST_CALL = 1;
    private static final int CAMERA_REQUEST = 1888;
    private static final int CAMERA_PERMISSION_CODE = 0;

    private boolean isEdit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isSelectebel(false);
        nameTextField = findViewById(R.id.name_text_field);
        phoneNumber = findViewById(R.id.phone_text_field);
        emailTextField = findViewById(R.id.email_text_field);
        saveEditButton = findViewById(R.id.save_edit_button);
        callButton = findViewById(R.id.call_button);
        emailButton = findViewById(R.id.email_button);
        saveFileButton = findViewById(R.id.save_file_button);
        loadFileButton = findViewById(R.id.load_file_button);
        avatarImage = findViewById(R.id.avatar);
        createPerson();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }
        if (!pathImage.isEmpty()){
            Bitmap bitmap = BitmapFactory.decodeFile(pathImage);
            avatarImage.setImageBitmap(bitmap);
        }




    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null) {
            Bitmap bitmap = savedInstanceState.getParcelable("image");
            avatarImage.setImageBitmap(bitmap);
            setColorToText();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        BitmapDrawable drawable = (BitmapDrawable) avatarImage.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        outState.putParcelable("image", bitmap);

        super.onSaveInstanceState(outState);
    }


    private void makePhoneCall(){
        String number = phoneNumber.getText().toString();
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL);
        }else {
            String dial = "tel:" + number;
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    makePhoneCall();
                } else {
                    Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show();
                }
                break;
            }case CAMERA_PERMISSION_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                }
                break;
            }
        }
    }

    public void onEditSaveButtonClick(View view){
        if (!isEdit) {
            saveEditButton.setText(R.string.edit_button_text);
            isSelectebel(isEdit);
            isEdit = true;
        }else {
            setColorToText();
            saveEditButton.setText(R.string.save_button_text);
            isSelectebel(isEdit);
            isEdit = false;
        }

        makeButtonVisible(isEdit);
    }

    private void setColorToText(){
        int textColor = getResources().getColor(R.color.blackColor);
        nameTextField.setTextColor(textColor);
        phoneNumber.setTextColor(textColor);
        emailTextField.setTextColor(textColor);
    }

    private void makeButtonVisible(boolean condition){
        if (condition){
            showButton();
        } else {
            hideButton();
        }
    }

    private void showButton(){
        emailButton.setVisibility(View.VISIBLE);
        callButton.setVisibility(View.VISIBLE);
        saveFileButton.setVisibility(View.VISIBLE);
        loadFileButton.setVisibility(View.VISIBLE);
    }

    private void hideButton() {
        emailButton.setVisibility(View.GONE);
        callButton.setVisibility(View.GONE);
        saveFileButton.setVisibility(View.GONE);
        loadFileButton.setVisibility(View.GONE);


    }

    public void onCallButtonClick(View view){
        String text = phoneNumber.getText().toString().trim();
        if (isEmpty(text)){
            PhoneDialogFragment dialogFragment = new PhoneDialogFragment();
            dialogFragment.show(getSupportFragmentManager(), "PhoneDialogFragment");
        } else {
            PhoneErrorDialogFragment errorDialog = new PhoneErrorDialogFragment();
            errorDialog.show(getSupportFragmentManager(), "PhoneErrorDialogFragment");
        }
    }

    public void onEmailButtonClick(View view){
        String email = emailTextField.getText().toString().trim();
        if (isEmpty(email)){
            EmailDialogFragment dialogFragment = new EmailDialogFragment();
            dialogFragment.show(getSupportFragmentManager(), "EmailDialogFragment");
        } else {
            EmailErrorDialogFragment errorDialog = new EmailErrorDialogFragment();
            errorDialog.show(getSupportFragmentManager(), "EmailErrorDialogFragment");
        }
    }

    private boolean isEmpty(String string){
        if (string.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    private void isSelectebel(boolean editable){
        nameTextField = findViewById(R.id.name_text_field);
        nameTextField.setEnabled(editable);
        phoneNumber = findViewById(R.id.phone_text_field);
        phoneNumber.setEnabled(editable);
        emailTextField = findViewById(R.id.email_text_field);
        emailTextField.setEnabled(editable);
    }

    private void sendMessage(String addressList){
        String[] address = addressList.split(",");
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, address);

        intent.setType("message/rfc822");
        startActivity(Intent.createChooser(intent, "Choose an email client"));
    }

    public void takeImageFromCamera(View view) {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case 10: {
                if (resultCode == RESULT_OK){
                    String path = data.getData().getPath();
                }
                break;
            }
            case CAMERA_REQUEST: {
                if (resultCode == RESULT_OK) {
                    Bitmap mphoto = (Bitmap) data.getExtras().get("data");
                    createImage(mphoto);
                    avatarImage.setImageBitmap(mphoto);
                }
                break;
            }

        }

        }

    private void createImage(Bitmap mphoto){

        String name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File photoFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), name);
        pathImage = photoFile.getAbsolutePath();
        try {
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(photoFile);
                mphoto.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            } finally {
                if (fos != null) fos.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(this, pathImage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void sendPhonePermission(boolean answer) {
        if (answer){
            makePhoneCall();
        }
    }

    @Override
    public void sendEmailPermission(boolean answer) {
        String email = emailTextField.getText().toString().trim();
        if (answer){
            sendMessage(email);
        }
    }

    public void onSaveButtonClick(View view){
        savePerson(person);
        String fileText = JsonObject.toJson(person);
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(fileName, MODE_PRIVATE);
            fos.write(fileText.getBytes());

            Toast.makeText(this, "saved to " + getFilesDir() + "/" + fileName, Toast.LENGTH_SHORT).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onLoadButtonClick(View view){
        setColorToText();
        FileInputStream fis = null;
        String text = "";
        try{
            fis = openFileInput(fileName);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            while((text = br.readLine())!=null){
                sb.append(text);
            }
            JsonObject.fromJsonToObject(person, sb.toString());
            nameTextField.setText(person.getName());
            phoneNumber.setText(person.getPhoneNumber());
            emailTextField.setText(person.getEmail());
            if (!person.getPhotoPass().isEmpty()){
                Bitmap bitmap = BitmapFactory.decodeFile(person.getPhotoPass());
                avatarImage.setImageBitmap(bitmap);
            }
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void createPerson(){
        String name = nameTextField.getText().toString();
        String email = emailTextField.getText().toString();
        String number = phoneNumber.getText().toString();
        String photoPath = pathImage;
        person = new Person(name, number, email, photoPath);
    }

   /* public void chooseFile(View view){
        filePickIntent = new Intent(Intent.ACTION_GET_CONTENT);
        filePickIntent.setType("/");
        startActivityForResult(filePickIntent, 10);
    }*/

    private void savePerson(Person person){
        String name = nameTextField.getText().toString();
        String email = emailTextField.getText().toString();
        String number = phoneNumber.getText().toString();
        person.setPhotoPass(pathImage);
        person.setPhoneNumber(number);
        person.setEmail(email);
        person.setName(name);
    }
}
