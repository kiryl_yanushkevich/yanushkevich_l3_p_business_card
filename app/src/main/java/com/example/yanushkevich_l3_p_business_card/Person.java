package com.example.yanushkevich_l3_p_business_card;

public class Person {
    private String email;
    private String phoneNumber;
    private String name;
    private String photoPass;

    public Person(String name, String phoneNumber, String email, String path){
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.photoPass = path;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoPass() {
        return photoPass;
    }

    public void setPhotoPass(String photoPass) {
        this.photoPass = photoPass;
    }
}
