package com.example.yanushkevich_l3_p_business_card;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonObject {
    public static String toJson(Person person){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("person_name", person.getName());
            jsonObject.put("person_number", person.getPhoneNumber());
            jsonObject.put("person_email", person.getEmail());
            jsonObject.put("person_photo_path", person.getPhotoPass());
            return jsonObject.toString();
        } catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public static void fromJsonToObject(Person person, String stringJson){
        try {
            JSONObject obj = new JSONObject(stringJson);
            person.setName(obj.getString("person_name"));
            person.setPhoneNumber(obj.getString("person_number"));
            person.setEmail(obj.getString("person_email"));
            person.setPhotoPass(obj.getString("person_photo_path"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
