package com.example.yanushkevich_l3_p_business_card;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class PhoneErrorDialogFragment extends DialogFragment {

    private static final String TAG = "PhoneErrorDialogFragment";



    private TextView title;


    private Button okButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_error_window, container, false);
        title = view.findViewById(R.id.error_dialog_window_title);
        okButton = view.findViewById(R.id.error_dialog_window_ok_button);
        String text = getResources().getText(R.string.phone_error_window_message).toString();
        title.setText(text);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        return view;
    }
}
